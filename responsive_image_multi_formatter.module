<?php

/**
 * @file custom theme function for the responsive image multi formatter.
 */

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Url;
use Drupal\responsive_image\Entity\ResponsiveImageMapping;
use Drupal\image\Plugin\Field\FieldType\ImageItem;


/**
 * Implements hook_theme().
 */
function responsive_image_multi_formatter_theme() {
  return array(
    'responsive_image_multi' => array(
      'variables' => array(
        'style_name' => NULL,
        'uri' => NULL,
        'width' => NULL,
        'height' => NULL,
        'alt' => '',
        'title' => NULL,
        'attributes' => array(),
        'mapping_id' => array(),
        'other_items' => array(),
      ),
      'function' => 'theme_responsive_image_multi',
    ),
    'responsive_image_multi_formatter' => array(
      'variables' => array(
        'item' => NULL,
        'path' => NULL,
        'image_style' => NULL,
        'mapping_id' => array(),
        'other_items' => array(),
      ),
      'function' => 'theme_responsive_image_multi_formatter',
    ),
  );
}


/**
 * Returns HTML for a responsive image multi field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - other_items: Other ImageItem objects to use as responsive alternative
 *   - image_style: An optional image style.
 *   - path: An optional array containing the link 'path' and link 'options'.
 *   - mapping_id: The ID of the responsive image mapping.
 *
 * @ingroup themeable
 * @return a renderable array
 */
function theme_responsive_image_multi_formatter($variables) {

  $item = $variables['item'];

  if (!isset($variables['mapping_id']) || empty($variables['mapping_id'])) {
    $image_formatter = array(
      '#theme' => 'image_formatter',
      '#item' => $item,
      '#image_style' => $variables['image_style'],
      '#path' => $variables['path'],
    );
    return drupal_render($image_formatter);
  }

  $other_items = array();

  foreach($variables['other_items'] as $key => $v) {

    /**
     * @var ImageItem $vitem
     */
    $vitem = $v['#item'];

    if (isset($vitem->uri)) {
      $other_items[$key]['uri'] = $vitem->uri;
    }
    elseif ($entity = $vitem->entity) {
      $other_items[$key]['uri'] = $entity->getFileUri();
      $other_items[$key]['entity'] = $entity;
    }
  }

  $responsive_image = array(
    '#theme' => 'responsive_image_multi',
    '#width' => $item->width,
    '#height' => $item->height,
    '#style_name' => $variables['image_style'],
    '#mapping_id' => $variables['mapping_id'],
    '#other_items' => $other_items,
  );
  if (isset($item->uri)) {
    $responsive_image['#uri'] = $item->uri;
  }
  elseif ($entity = $item->entity) {
    $responsive_image['#uri'] = $entity->getFileUri();
    $responsive_image['#entity'] = $entity;
  }
  $responsive_image['#alt'] = $item->alt;
  if (Unicode::strlen($item->title) != 0) {
    $responsive_image['#title'] = $item->title;
  }
  // @todo Add support for route names.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    $options['html'] = TRUE;
    return \Drupal::l($responsive_image, Url::fromUri($path, $options));
  }

  return drupal_render($responsive_image);

}

/**
 * Returns HTML for a responsive multi image.
 *
 * @param $variables
 *   An associative array containing:
 *   - uri: Either the path of the image file (relative to base_path()) or a
 *     full URL.
 *   - width: The width of the image (if known).
 *   - height: The height of the image (if known).
 *   - alt: The alternative text for text-based browsers.
 *   - title: The title text is displayed when the image is hovered in some
 *     popular browsers.
 *   - style_name: The name of the style to be used as a fallback image.
 *   - mapping_id: The ID of the responsive image mapping.
 *
 * @ingroup themeable
 *
 * @return a renderable array
 */
function theme_responsive_image_multi($variables) {
  // Make sure that width and height are proper values
  // If they exists we'll output them
  // @see http://www.w3.org/community/respimg/2012/06/18/florians-compromise/
  if (isset($variables['width']) && empty($variables['width'])) {
    unset($variables['width']);
    unset($variables['height']);
  }
  elseif (isset($variables['height']) && empty($variables['height'])) {
    unset($variables['width']);
    unset($variables['height']);
  }

  $sources = array();

  // Fallback image, output as source with media query.
  $sources[] = array(
    'src' => _responsive_image_image_style_url($variables['style_name'], $variables['uri']),
    'dimensions' => responsive_image_get_image_dimensions($variables),
  );
  $responsive_image_mapping = ResponsiveImageMapping::load($variables['mapping_id']);
  // All breakpoints and multipliers.

  $i_count = -1;
  $last_uri = $variables['uri'];

  $breakpoints = \Drupal::service('breakpoint.manager')->getBreakpointsByGroup($responsive_image_mapping->getBreakpointGroup());
  foreach ($responsive_image_mapping->getKeyedMappings() as $breakpoint_id => $multipliers) {
    if (isset($breakpoints[$breakpoint_id])) {
      $breakpoint = $breakpoints[$breakpoint_id];
      $new_sources = array();
      foreach ($multipliers as $multiplier => $image_style) {
        $new_source = $variables;
        $new_source['style_name'] = $image_style;
        $new_source['#multiplier'] = $multiplier;
        $new_sources[] = $new_source;
      }

      // Only one image, use src.
      if (count($new_sources) == 1) {

        // if there is an other image set for this breakpoint, use it
        if(isset($variables['other_items'][$i_count])) {
          $new_sources[0]['uri'] = $variables['other_items'][$i_count]['uri'];
          $last_uri = $new_sources[0]['uri'];

        // if there is no image set for this breakpoint, use the last one
        } else {
          $new_sources[0]['uri'] = $last_uri;
        }

        $sources[] = array(
          'src' => _responsive_image_image_style_url($new_sources[0]['style_name'], $new_sources[0]['uri']),
          'dimensions' => responsive_image_get_image_dimensions($new_sources[0]),
          'media' => $breakpoint->getMediaQuery(),
        );
      }
      else {
        // Multiple images, use srcset.
        $srcset = array();
        foreach ($new_sources as $new_source) {
          $srcset[] = _responsive_image_image_style_url($new_source['style_name'], $new_source['uri']) . ' ' . $new_source['#multiplier'];
        }
        $sources[] = array(
          'srcset' => implode(', ', $srcset),
          'dimensions' => responsive_image_get_image_dimensions($new_sources[0]),
          'media' => $breakpoint->getMediaQuery(),
        );
      }
    }

    $i_count++;
  }

  if (!empty($sources)) {
    $output = array();
    $output[] = '<picture>';

    // Add source tags to the output.
    foreach ($sources as $source) {
      $responsive_image_source = array(
        '#theme' => 'responsive_image_source',
        '#src' => $source['src'],
        '#dimensions' => $source['dimensions'],
      );
      if (isset($source['media'])) {
        $responsive_image_source['#media'] = $source['media'];
      }
      if (isset($source['srcset'])) {
        $responsive_image_source['#srcset'] = $source['srcset'];
      }
      $output[] = drupal_render($responsive_image_source);
    }

    $output[] = '</picture>';
    return SafeMarkup::set(implode("\n", $output));
  }
}