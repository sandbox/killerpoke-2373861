<?php

/**
 * @file
 * Contains \Drupal\responsive_image_multi_formatter\Plugin\field\formatter\ResponsiveImageMultiFormatter.
 */

namespace Drupal\responsive_image_multi_formatter\Plugin\Field\FieldFormatter;


use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Plugin for responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_image_multi",
 *   label = @Translation("Responsive image multi"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class ResponsiveImageMultiFormatter extends ResponsiveImageFormatter {
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = parent::viewElements($items);

    if(count($elements) > 1) {

      // we use the first image as base image
      $first = array_shift($elements);

      // and addthe other items as variable to the first one
      $first['#other_items'] = $elements;

      // use our custom theme function
      $first['#theme'] = 'responsive_image_multi_formatter';

      return array($first);

    } else {
      return $elements;
    }
  }
}
